#include <iostream>
#include "parser.h"
#include <stdexcept>
/*DCPU-16 assembler*/

using namespace std;

int main(int argc, char *argv[])
{
    try
    {
        Parser parser(argv[1]);
        parser.parse();
    }
    catch(std::runtime_error &e)
    {
        cerr << "[Erreur] " << e.what() << std::endl;
    }

    return 0;
}

