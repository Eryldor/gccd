#ifndef PARSER_H
#define PARSER_H

#include <string>
#include "lexer.h"
#include <vector>

class Parser
{
public:
    Parser();
    Parser(std::string filename);

    void parse(); //Parse code and display the token
    void analyse_syntax();

    void analyse_basic_instruction(Lexer l, int &i);

private :

    bool checkBracketSyntax(Lexer l, int &i);
    void operandeCode(Lexer l, bool bracket, unsigned short &oper, bool &needValueOnNextWord, bool allowLiteralValue = false);
    short convertLexerToNumber(Lexer l);

    Lexer m_lexer;

    std::vector<Lexer> m_lexerList;

    std::vector<short> m_binCode;
};

#endif // PARSER_H
