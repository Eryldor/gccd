#ifndef FILEBUFFER_H
#define FILEBUFFER_H

#include <sstream>
#include <string>
#include <fstream>

#define NEWLINE '\n'
#define COMMENT ';'
#define HEX_SEPARATOR 'x'
#define UNDERSCORE '_'

//Specials symboles

#define SEPARATOR ','
#define OPEN_BRACKET '['
#define CLOSE_BRACKET ']'
#define QUOTE '"'
#define ADD_S '+'
#define LABEL ':'

class FileBuffer
{
public:
    FileBuffer();
    FileBuffer(std::string filename);

    bool good();
    char peek();
    bool next(char &ch);
    void reset();

    bool operator>>(char &c);

    int line()
    {
        return m_ln;
    }

private:
    std::string m_filename;
    std::stringstream m_buffer;

    char m_ch;
    int m_ln;
};

#endif // FILEBUFFER_H
