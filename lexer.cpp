#include "lexer.h"
#include <stdexcept>
#include <iostream>

std::map<int,std::string> m_registerSymbol;
std::map<int,std::string> m_sysRegisterSymbol;
std::map<int,std::string> m_basicOperationSymbol;
std::map<int,std::string> m_nonBasicOperationSymbol;
std::map<int,std::string> m_stackOperationSymbol;
std::map<int,std::string> m_preprocessorSymbol;


#define INIT_TOKEN(a) m_tokenType[a] = #a
#define INIT_BASIC_INSTRCUTION(a) m_basicOperationSymbol[a] = #a
#define INIT_REGISTER(a) m_registerSymbol[a] = #a
#define INIT_STACK_OPERATION(a) m_stackOperationSymbol[a] = #a

Lexer::Lexer()
{
}

Lexer::Lexer(std::string filename) : m_buffer(filename)
{
}

Lexer::Lexer(const Lexer &other)
{
    m_type = other.m_type;
    m_texte = other.m_texte;
    m_line = other.m_line;
}

bool Lexer::hasNext()
{
    return m_type != TYPE_END;
}

void Lexer::next()
{
    skip_whitespace();

    char c = m_buffer.peek();

    if(!m_buffer.good())
    {
        m_type = TYPE_END;
        return;
    }

    if(isalpha(c))
        phrase();
    else if(isdigit(c))
        number();
    else
        symbol();

    m_line = m_buffer.line();
}

void Lexer::skip_whitespace()
{
    char ch = m_buffer.peek();

    if(!m_buffer.good())
        return;

    while(isspace(ch)) //On zappe tant que l'on a des espaces
        if(!(m_buffer >> ch))
            return;

    if(ch == COMMENT) //On est sur une ligne de commentaire
    {
        while(ch != NEWLINE) //On zappe jusqu'a la fin de la ligne
        {
            if(!(m_buffer >> ch))
                return;
        }

        skip_whitespace(); //On enleve les espace en debut de ligne
    }
}

void Lexer::phrase()
{
    m_texte.clear();
    char ch = m_buffer.peek();
    m_texte += ch;
    while(m_buffer >> ch && (isalnum(ch) || ch == UNDERSCORE))
        m_texte += ch;

    if(is_register())
        m_type = TYPE_REGISTER;
    else if(is_sysRegister())
        m_type = TYPE_SYS_REGISTER;
    else if(is_basicOperation())
        m_type = TYPE_BASIC_INSTRUCTION;
    else if(is_nonBasicOperation())
        m_type = TYPE_NON_NASIC_INSTRUCTION;
    else if(is_stackOperation())
        m_type = TYPE_STACK_OPERATION;
    else if(is_preprocessor())
        m_type = TYPE_PREPROCESSOR;
    else
        m_type = TYPE_NAME;

    if(m_type != TYPE_NAME && m_type != TYPE_UNKNOW)
        m_texte = to_uppercase(m_texte);
}

void Lexer::number()
{
    char ch = m_buffer.peek();
    m_texte.clear();
    m_type = TYPE_NUMBER;

    m_texte += ch;

    while(isdigit(ch))
    {
        if(!(m_buffer >> ch))
            return;
        m_texte += ch;
    }

    if(ch == HEX_SEPARATOR) //Commence par 0x... (mais pas toujours; 2xFF passe aussi)
    {
        m_texte.clear();
        if(!(m_buffer >> ch) || !is_hex(ch))
        {
            std::stringstream ss;
            ss << "(line " << m_buffer.line() << ") L'expression n'est pas un nombre valide";
            throw(std::runtime_error(ss.str()));
        }

        m_texte += ch;

        while(m_buffer >> ch && is_hex(ch))
        {
            m_texte += ch;
        }

        m_type = TYPE_HEX_NUMBER;
    }
}

void Lexer::symbol()
{
    m_texte.clear();
    char ch = m_buffer.peek();

    switch(ch)
    {
    case SEPARATOR:
        m_texte += ch;
        m_type = TYPE_SEPARATOR;
        break;
    case OPEN_BRACKET:
        m_texte += ch;
        m_type = TYPE_OPEN_BRAKET;
        break;
    case CLOSE_BRACKET:
        m_texte += ch;
        m_type = TYPE_CLOSE_BRACKET;
        break;
    case QUOTE:
        while(m_buffer >> ch && ch != QUOTE)
        {
            m_texte += ch;
        }

        if(ch != QUOTE)
        {
            std::stringstream ss;
            ss << "(ligne " << m_buffer.line() << ") \" manquant";
            throw(std::runtime_error(ss.str()));
        }

        m_type = TYPE_STRING;
        break;
    case ADD_S:
        m_texte += ch;
        m_type = TYPE_ADD;
        break;
    case LABEL:
        m_texte += ch;
        m_type = TYPE_LABEL;
        break;
    default:
        m_type = TYPE_UNKNOW;
    }
    m_buffer.next(ch);
}

bool Lexer::is_hex(char c)
{
    return isdigit(c) || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
}

bool Lexer::is_register()
{
    for(std::map<int, std::string>::const_iterator i = m_registerSymbol.begin() ; i != m_registerSymbol.end() ; ++i)
        if(i->second == to_uppercase(m_texte))
            return true;
    return false;
}

bool Lexer::is_sysRegister()
{
    for(std::map<int, std::string>::const_iterator i = m_sysRegisterSymbol.begin() ; i != m_sysRegisterSymbol.end() ; ++i)
        if(i->second == to_uppercase(m_texte))
            return true;
    return false;
}

bool Lexer::is_basicOperation()
{
    for(std::map<int, std::string>::const_iterator i = m_basicOperationSymbol.begin() ; i != m_basicOperationSymbol.end() ; ++i)
        if(i->second == to_uppercase(m_texte))
            return true;
    return false;
}

bool Lexer::is_preprocessor()
{
    for(std::map<int, std::string>::const_iterator i = m_preprocessorSymbol.begin() ; i != m_preprocessorSymbol.end() ; ++i)
        if(i->second == to_uppercase(m_texte))
            return true;
    return false;
}

bool Lexer::is_stackOperation()
{
    for(std::map<int, std::string>::const_iterator i = m_stackOperationSymbol.begin() ; i != m_stackOperationSymbol.end() ; ++i)
        if(i->second == to_uppercase(m_texte))
            return true;
    if(to_uppercase(m_texte) == "POP")
        return true;
    return false;
}

bool Lexer::is_nonBasicOperation()
{
    for(std::map<int, std::string>::const_iterator i = m_nonBasicOperationSymbol.begin() ; i != m_nonBasicOperationSymbol.end() ; ++i)
        if(i->second == to_uppercase(m_texte))
            return true;
    return false;
}

std::string Lexer::to_uppercase(std::string src)
{
    for(unsigned int i = 0 ; i < src.size() ; i++)
    {
        if(isalpha(src[i]))
            src[i] = toupper(src[i]);
    }

    return src;
}

void init_token_type()
{
    INIT_TOKEN(TYPE_LABEL);
    INIT_TOKEN(TYPE_BASIC_INSTRUCTION);
    INIT_TOKEN(TYPE_NON_NASIC_INSTRUCTION);
    INIT_TOKEN(TYPE_REGISTER);
    INIT_TOKEN(TYPE_SYS_REGISTER);
    INIT_TOKEN(TYPE_PREPROCESSOR);
    INIT_TOKEN(TYPE_STACK_OPERATION);
    INIT_TOKEN(TYPE_NUMBER);
    INIT_TOKEN(TYPE_HEX_NUMBER);
    INIT_TOKEN(TYPE_SEPARATOR);
    INIT_TOKEN(TYPE_STRING);
    INIT_TOKEN(TYPE_OPEN_BRAKET);
    INIT_TOKEN(TYPE_CLOSE_BRACKET);
    INIT_TOKEN(TYPE_NAME);
    INIT_TOKEN(TYPE_ADD);
    INIT_TOKEN(TYPE_UNKNOW);
    INIT_TOKEN(TYPE_END);
}

void init_symbols()
{

    INIT_REGISTER(A);
    INIT_REGISTER(B);
    INIT_REGISTER(C);
    INIT_REGISTER(X);
    INIT_REGISTER(Y);
    INIT_REGISTER(Z);
    INIT_REGISTER(I);
    INIT_REGISTER(J);

    INIT_BASIC_INSTRCUTION(SET);
    INIT_BASIC_INSTRCUTION(ADD);
    INIT_BASIC_INSTRCUTION(SUB);
    INIT_BASIC_INSTRCUTION(MUL);
    INIT_BASIC_INSTRCUTION(MLI);
    INIT_BASIC_INSTRCUTION(DIV);
    INIT_BASIC_INSTRCUTION(DVI);
    INIT_BASIC_INSTRCUTION(MOD);
    INIT_BASIC_INSTRCUTION(MDI);
    INIT_BASIC_INSTRCUTION(AND);
    INIT_BASIC_INSTRCUTION(BOR);
    INIT_BASIC_INSTRCUTION(XOR);
    INIT_BASIC_INSTRCUTION(SHR);
    INIT_BASIC_INSTRCUTION(ASR);
    INIT_BASIC_INSTRCUTION(SHL);
    INIT_BASIC_INSTRCUTION(IFB);
    INIT_BASIC_INSTRCUTION(IFC);
    INIT_BASIC_INSTRCUTION(IFE);
    INIT_BASIC_INSTRCUTION(IFN);
    INIT_BASIC_INSTRCUTION(IFG);
    INIT_BASIC_INSTRCUTION(IFA);
    INIT_BASIC_INSTRCUTION(IFL);
    INIT_BASIC_INSTRCUTION(IFU);
    INIT_BASIC_INSTRCUTION(ADX);
    INIT_BASIC_INSTRCUTION(SBX);
    INIT_BASIC_INSTRCUTION(STI);
    INIT_BASIC_INSTRCUTION(STD);

    INIT_STACK_OPERATION(PUSH);
    INIT_STACK_OPERATION(PEEK);
    INIT_STACK_OPERATION(PICK);
    //INIT_STACK_OPERATION(POP); //Geré autrement car opcode push = opcode pop (depend de la place dans l'instruction)
}

int Lexer::getToken()
{
    return m_type;
}

std::string Lexer::getText()
{
    return m_texte;
}

Lexer &Lexer::operator =(const Lexer &other)
{
    m_type = other.m_type;
    m_texte = other.m_texte;
    m_line = other.m_line;
}
