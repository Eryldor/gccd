TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.cpp \
    lexer.cpp \
    filebuffer.cpp \
    parser.cpp

HEADERS += \
    lexer.h \
    filebuffer.h \
    parser.h

