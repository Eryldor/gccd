#include "parser.h"
#include <iostream>
#include <stdexcept>
#include <cstdlib>

#define THROW throw(std::runtime_error(ss.str()));

#define CHECK_EO_ARRAY(a) if(a >= m_lexerList.size()) \
{ \
    std::stringstream ss; \
    ss << "(ligne " << l.line() << ") fin de fichier innatendue"; \
    THROW \
}

#define ERROR(a) std::stringstream ss; \
    ss << "(ligne " << l.line() << ") " << a; \
    THROW

Parser::Parser()
{
}

Parser::Parser(std::string filename) : m_lexer(filename)
{

}

void Parser::parse()
{
    init_symbols();
    init_token_type();
    m_lexer.next();
    while(m_lexer.hasNext())
    {
        m_lexerList.push_back(m_lexer);
        m_lexer.next();
    }

    analyse_syntax();

    for(int i = 0 ; i < m_binCode.size() ; i++)
    {
        std::cout << std::hex << m_binCode.at(i) << " ";
    }

    std::cout << std::dec << std::endl;
}

void Parser::analyse_syntax()
{
    m_binCode.clear();

    for(int i = 0 ; i < m_lexerList.size() ;)
    {
        Lexer l = m_lexerList[i];
        switch(l.getToken())
        {
        case TYPE_BASIC_INSTRUCTION:
            analyse_basic_instruction(l, i);
            break;
        default:
        {
            ERROR("L'espression ne peut se situer en début de ligne ('" << l.getText() << "')")
        }
        }
    }
}

void Parser::analyse_basic_instruction(Lexer l, int &i)
{
    if(i + 3 >= m_lexerList.size())
    {
        ERROR("Fin de fichier innattendue")
    }

    Lexer a, sep, b, offset;

    i++;

    a = m_lexerList[i];

    bool op_a_bracket = checkBracketSyntax(a, i);
    if(op_a_bracket)
    {
        a = m_lexerList[i];
        i++; //On zappe le ']'
    }

    i++;

    if(a.getToken() != TYPE_HEX_NUMBER && a.getToken() != TYPE_NUMBER && a.getToken() != TYPE_REGISTER && a.getToken()!= TYPE_SYS_REGISTER && a.getToken() != TYPE_STACK_OPERATION)
    {
        ERROR("Expression '" << a.getText() << "' innattendue apres '" << l.getText() << "'")
    }

    if(a.getToken() == TYPE_STACK_OPERATION)
    {
        if(a.getText() != "PUSH")
        {
            ERROR("Instruction illegal en a : '" << a.getText() << "'")
        }
    }

    CHECK_EO_ARRAY(i)

    sep = m_lexerList.at(i);

    if(sep.getToken() != TYPE_SEPARATOR)
    {
        ERROR("',' attendue a la place de '" << sep.getText() << "'")
    }

    i++;

    CHECK_EO_ARRAY(i)

    b = m_lexerList.at(i);
    bool op_b_bracket = checkBracketSyntax(b, i);
    if(op_b_bracket)
    {
        b = m_lexerList[i];
        i++;
    }

    if(b.getToken() == TYPE_STACK_OPERATION)
    {
        if(b.getText() == "PUSH")
        {
            ERROR("Instruction illegal en b : '" << b.getText() << "'")
        }

        if(b.getText() == "PICK")
        {
            CHECK_EO_ARRAY(i+1)
            offset = m_lexerList.at(i+1);
            if(offset.getToken() != TYPE_NUMBER && offset.getToken() != TYPE_HEX_NUMBER)
            {
                ERROR("Nombre entier attendue apres PICK")
            }
            i++;
        }
    }

    i++;

    unsigned short code = 0x0, opcode = 0x0, a_op = 0x0, b_op = 0x0;
    bool needValAOnNextW = false, needValBOnNextW = false;

    for(std::map<int, std::string>::const_iterator i = m_basicOperationSymbol.begin() ; i != m_basicOperationSymbol.end() ; ++i)
    {
        if(i->second == l.to_uppercase(l.getText()))
            opcode = i->first;
    }

    operandeCode(a, op_a_bracket, a_op, needValAOnNextW, true);
    operandeCode(b, op_b_bracket, b_op, needValBOnNextW);

    code = opcode & 0x1F;
    code |= (b_op << 5) & 0x3e0;
    code |= (a_op << 10) & 0xfc00;

    m_binCode.push_back(code);

    if(needValAOnNextW)
    {
        m_binCode.push_back(convertLexerToNumber(a));
    }

    if(needValBOnNextW)
    {
        m_binCode.push_back(convertLexerToNumber(b));
    }

    if(b.getText() == "PICK")
    {
        m_binCode.push_back(convertLexerToNumber(offset));
    }
}

bool Parser::checkBracketSyntax(Lexer l, int &i)
{
    if(l.getToken() != TYPE_OPEN_BRAKET)
        return false;

    CHECK_EO_ARRAY(i+1)
    CHECK_EO_ARRAY(i+2)

            if(m_lexerList.at(i+1).getToken() != TYPE_NUMBER && m_lexerList.at(i+1).getToken() != TYPE_HEX_NUMBER && m_lexerList.at(i+1).getToken() != TYPE_REGISTER)
    {
        ERROR("Valeur numérique ou registre attendue entre '[' et ']'")
    }

    if(m_lexerList.at(i+2).getToken() != TYPE_CLOSE_BRACKET)
    {
        ERROR("']' manquant")
    }

    i++;
    return true;
}

void Parser::operandeCode(Lexer l, bool bracket, unsigned short &oper, bool &needValueOnNextWord, bool allowLitteralValue)
{
    if(l.getToken() == TYPE_REGISTER)
    {
        for(std::map<int, std::string>::const_iterator i = m_registerSymbol.begin() ; i != m_registerSymbol.end() ; ++i)
        {
            if(l.to_uppercase(l.getText()) == i->second)
            {
                oper = i->first;
                if(bracket)
                    oper += 0x8;
                needValueOnNextWord= false;
                break;
            }
        }
    }

    if(l.getToken() == TYPE_NUMBER || l.getToken() == TYPE_HEX_NUMBER)
    {
        short num = convertLexerToNumber(l);

        if(bracket)
        {
            oper = 0x1e;
            needValueOnNextWord = true;
        }
        else
        {
            if(num >= -1 && num <= 30)
            {
                oper = num + 0x20;
                needValueOnNextWord = false;
            }
            else
            {
                oper = 0x1f;
                needValueOnNextWord = true;
            }
        }
    }

    if(l.getToken() == TYPE_STACK_OPERATION)
    {
        if(l.getText() == "POP")
            oper = 0x18;

        for(std::map<int, std::string>::const_iterator i = m_stackOperationSymbol.begin() ; i != m_stackOperationSymbol.end() ; ++i)
        {
            if(i->second == l.to_uppercase(l.getText()))
            {
                oper = i->first;
                needValueOnNextWord = false;
            }
        }
    }
}

short Parser::convertLexerToNumber(Lexer l)
{
    std::stringstream num(l.getText());
    short result = 0;
    switch(l.getToken())
    {
    case TYPE_NUMBER:
        num >> result;
        break;
    case TYPE_HEX_NUMBER:
        num >> std::hex >> result;
        break;
    default:
        ERROR("Tentative de conversion d'un nombre qui n'en n'est pas un :'" << l.getText() << "'")
    }

    return result;
}
