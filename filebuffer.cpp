#include "filebuffer.h"
#include <stdexcept>

FileBuffer::FileBuffer()
{
    m_ln = 1;
}

FileBuffer::FileBuffer(std::string filename) : m_filename(filename)
{
    std::ifstream file;
    file.open(m_filename.c_str());
    if(!file)
        throw(std::runtime_error("Impossible d'ouvrir le fichier source"));

    m_buffer << file.rdbuf();
    reset();
}

char FileBuffer::peek()
{
    return m_ch;
}

bool FileBuffer::good()
{
    return m_buffer.good();
}

bool FileBuffer::next(char &ch)
{
    if(!good())
        return false;
    m_buffer.seekg(1, std::ios_base::cur);
    char c = m_buffer.peek();
    if(c == NEWLINE)
        m_ln++;
    ch = c;
    m_ch = c;
    return true;
}

void FileBuffer::reset()
{
    m_ln = 1;
    m_buffer.seekg(0, std::ios::beg);
    m_ch = m_buffer.peek();
}

bool FileBuffer::operator >>(char &c)
{
    return next(c);
}
