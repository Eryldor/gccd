#ifndef LEXER_H
#define LEXER_H

#include <string>
#include <set>
#include <map>
#include "filebuffer.h"

extern std::map<int,std::string> m_registerSymbol;
extern std::map<int,std::string> m_sysRegisterSymbol;
extern std::map<int,std::string> m_basicOperationSymbol;
extern std::map<int,std::string> m_nonBasicOperationSymbol;
extern std::map<int,std::string> m_stackOperationSymbol;
extern std::map<int,std::string> m_preprocessorSymbol;

static std::map<int, std::string> m_tokenType;
void init_symbols();
void init_token_type();

enum Token_type
{
    TYPE_LABEL,
    TYPE_BASIC_INSTRUCTION,
    TYPE_NON_NASIC_INSTRUCTION,
    TYPE_REGISTER,
    TYPE_SYS_REGISTER,
    TYPE_PREPROCESSOR,
    TYPE_STACK_OPERATION,
    TYPE_NUMBER,
    TYPE_HEX_NUMBER,
    TYPE_SEPARATOR,
    TYPE_STRING,
    TYPE_OPEN_BRAKET,
    TYPE_CLOSE_BRACKET,
    TYPE_NAME,
    TYPE_ADD,
    TYPE_UNKNOW,
    TYPE_END
};

enum Basic_instruction
{
    SET = 1,
    ADD,
    SUB,
    MUL,
    MLI,
    DIV,
    DVI,
    MOD,
    MDI,
    AND,
    BOR,
    XOR,
    SHR,
    ASR,
    SHL,
    IFB,
    IFC,
    IFE,
    IFN,
    IFG,
    IFA,
    IFL,
    IFU,
    UNK_0x18,
    UNK_0x19,
    ADX,
    SBX,
    UNK_0x1c,
    UNK_0x1d,
    STI,
    STD
};

enum Register
{
    A,
    B,
    C,
    X,
    Y,
    Z,
    I,
    J
};

enum Stack_operation
{
    PUSH = 0x18,
    PEEK = 0x19,
    PICK = 0x1A,
    POP = 0x18
};

class Lexer
{
public:
    Lexer();
    Lexer(std::string filename);
    Lexer(const Lexer &other);

    bool hasNext();
    void next();

    void phrase();
    void number();
    void symbol();

    void skip_whitespace();

    bool is_hex(char c);
    bool is_register();
    bool is_sysRegister();
    bool is_basicOperation();
    bool is_nonBasicOperation();
    bool is_stackOperation();
    bool is_preprocessor();

    std::string to_uppercase(std::string src);
    std::string token()
    {
        return m_tokenType[m_type];
    }

    int getToken();
    std::string getText();
    int line()
    {
        return m_line;
    }

    Lexer &operator=(const Lexer& other);

private:
    FileBuffer m_buffer;

    Token_type m_type;

    std::string m_texte;
    int m_line;
};

#endif // LEXER_H
